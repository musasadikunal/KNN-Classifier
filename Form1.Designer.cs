﻿namespace knn1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_dosya_h = new System.Windows.Forms.TextBox();
            this.button_yukle_h = new System.Windows.Forms.Button();
            this.textBox_kdegeri = new System.Windows.Forms.TextBox();
            this.button_yukle_i = new System.Windows.Forms.Button();
            this.textBox_dosya_i = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.label_sonuc = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label_maxk = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.button_sonuc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_dosya_h
            // 
            this.textBox_dosya_h.Location = new System.Drawing.Point(96, 22);
            this.textBox_dosya_h.Name = "textBox_dosya_h";
            this.textBox_dosya_h.Size = new System.Drawing.Size(242, 20);
            this.textBox_dosya_h.TabIndex = 0;
            // 
            // button_yukle_h
            // 
            this.button_yukle_h.Location = new System.Drawing.Point(344, 20);
            this.button_yukle_h.Name = "button_yukle_h";
            this.button_yukle_h.Size = new System.Drawing.Size(75, 23);
            this.button_yukle_h.TabIndex = 1;
            this.button_yukle_h.Text = "Yükle";
            this.button_yukle_h.UseVisualStyleBackColor = true;
            this.button_yukle_h.Click += new System.EventHandler(this.button_yukle_h_Click);
            // 
            // textBox_kdegeri
            // 
            this.textBox_kdegeri.Location = new System.Drawing.Point(96, 52);
            this.textBox_kdegeri.Name = "textBox_kdegeri";
            this.textBox_kdegeri.Size = new System.Drawing.Size(242, 20);
            this.textBox_kdegeri.TabIndex = 2;
            // 
            // button_yukle_i
            // 
            this.button_yukle_i.Location = new System.Drawing.Point(344, 81);
            this.button_yukle_i.Name = "button_yukle_i";
            this.button_yukle_i.Size = new System.Drawing.Size(75, 23);
            this.button_yukle_i.TabIndex = 7;
            this.button_yukle_i.Text = "Yükle";
            this.button_yukle_i.UseVisualStyleBackColor = true;
            this.button_yukle_i.Click += new System.EventHandler(this.button_yukle_i_Click);
            // 
            // textBox_dosya_i
            // 
            this.textBox_dosya_i.Location = new System.Drawing.Point(96, 81);
            this.textBox_dosya_i.Name = "textBox_dosya_i";
            this.textBox_dosya_i.Size = new System.Drawing.Size(242, 20);
            this.textBox_dosya_i.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Dosya Hazır:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "K Değeri:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Dosya İstenen:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Sonuç: ";
            // 
            // label_sonuc
            // 
            this.label_sonuc.AutoSize = true;
            this.label_sonuc.Location = new System.Drawing.Point(267, 128);
            this.label_sonuc.Name = "label_sonuc";
            this.label_sonuc.Size = new System.Drawing.Size(0, 13);
            this.label_sonuc.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(341, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Max K: ";
            // 
            // label_maxk
            // 
            this.label_maxk.AutoSize = true;
            this.label_maxk.Location = new System.Drawing.Point(390, 59);
            this.label_maxk.Name = "label_maxk";
            this.label_maxk.Size = new System.Drawing.Size(0, 13);
            this.label_maxk.TabIndex = 15;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(434, 20);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(84, 184);
            this.richTextBox1.TabIndex = 16;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(544, 20);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(121, 184);
            this.richTextBox2.TabIndex = 17;
            this.richTextBox2.Text = "";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(684, 20);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(84, 184);
            this.richTextBox3.TabIndex = 18;
            this.richTextBox3.Text = "";
            // 
            // button_sonuc
            // 
            this.button_sonuc.Location = new System.Drawing.Point(12, 123);
            this.button_sonuc.Name = "button_sonuc";
            this.button_sonuc.Size = new System.Drawing.Size(168, 23);
            this.button_sonuc.TabIndex = 19;
            this.button_sonuc.Text = "Sonuç";
            this.button_sonuc.UseVisualStyleBackColor = true;
            this.button_sonuc.Click += new System.EventHandler(this.buton_sonuc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 215);
            this.Controls.Add(this.button_sonuc);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label_maxk);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label_sonuc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_yukle_i);
            this.Controls.Add(this.textBox_dosya_i);
            this.Controls.Add(this.textBox_kdegeri);
            this.Controls.Add(this.button_yukle_h);
            this.Controls.Add(this.textBox_dosya_h);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_dosya_h;
        private System.Windows.Forms.Button button_yukle_h;
        private System.Windows.Forms.TextBox textBox_kdegeri;
        private System.Windows.Forms.Button button_yukle_i;
        private System.Windows.Forms.TextBox textBox_dosya_i;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_sonuc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_maxk;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Button button_sonuc;
    }
}

